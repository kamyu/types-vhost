import express from 'express';
declare function vhost(hostname : string | RegExp, handle : express.RequestHandler) : express.RequestHandler;
export = vhost